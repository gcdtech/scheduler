# Change log

### 1.0.1
* Changed: service custard command now accepts i/o stream parameters. 
* Improved: refactored into service command into reusable traits.

### 1.0.0
* Initial release
