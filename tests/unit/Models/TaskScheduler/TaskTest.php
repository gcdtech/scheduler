<?php

namespace GcdTech\Scheduler\Tests\Models\TaskScheduler;

use Codeception\TestCase\Test;
use GcdTech\Scheduler\Models\SchedulerSchema;
use GcdTech\Scheduler\SchedulerModule;
use Rhubarb\Crown\Application;
use Rhubarb\Crown\Layout\LayoutModule;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use GcdTech\Scheduler\Models\Task;
use GcdTech\Scheduler\Models\TaskLog;
use GcdTech\Scheduler\TaskSchedulerSettings;
use Rhubarb\Stem\Repositories\Offline\Offline;
use Rhubarb\Stem\Repositories\Repository;
use Rhubarb\Stem\Schema\SolutionSchema;

/**
 * Class TaskTest
 */
class TaskTest extends Test
{
    private static $processedValue;

    protected function _before()
    {
        parent::_before();

        Repository::setDefaultRepositoryClassName(Offline::class);
        SolutionSchema::registerSchema('Scheduler', SchedulerSchema::class);

        $app = new Application();
        $app->registerModule(new SchedulerModule());
        $app->unitTesting = true;
        $app->initialiseModules();

        LayoutModule::disableLayout();
    }

    public function testInstallTask()
    {
        $dateTime = new \DateTime();
        Task::installTask('TestMethod', 'P1D', $dateTime);

        $task = Task::findLast();
        $this->assertEquals($task->MethodName, 'TestMethod');
        $this->assertEquals($task->DateInterval, 'P1D');
        $this->assertEquals($task->NextRun, $dateTime);
        $this->assertEquals($task->IntervalType, Task::INTERVAL_FROM_TASK_COMPLETED);
    }

    public function testTaskProcessed()
    {
        $dateTime = new \DateTime();
        Task::installTask('\GcdTech\Scheduler\Tests\Models\TaskScheduler\TaskTest::testMethod', 'P1D', $dateTime);
        Task::processTasks();

        $this->assertEquals('processed', self::$processedValue);
    }

    public function testTaskSuccess()
    {
        $dateTime = new \DateTime();
        $originalDate = clone $dateTime;
        $task = Task::installTask('\GcdTech\Scheduler\Tests\Models\TaskScheduler\TaskTest::testMethod', 'P1D', $dateTime);
        Task::processTasks();

        $log = TaskLog::findLast();
        $this->assertTrue($log->Completed);

        $task->NextRun = $originalDate;
        // rhubarb date comparison bug, force save
        $task->save(true);

        Task::processTasks();

        $this->assertNotEquals($log->getUniqueIdentifier(), TaskLog::findLast()->getUniqueIdentifier());
    }

    public function testTaskFailure()
    {
        $dateTime = new \DateTime();
        Task::installTask('\GcdTech\Scheduler\Tests\Models\TaskScheduler\TaskTest::taskFails', 'P1D', $dateTime);
        Task::processTasks();

        $this->assertFalse(TaskLog::findLast()->Completed);
        $this->assertEquals(1, TaskLog::findLast()->Attempts);

        Task::processTasks();
        $this->assertEquals(2, TaskLog::findLast()->Attempts);
    }

    public function testRestrictedAttempts()
    {
        Task::installTask('\GcdTech\Scheduler\Tests\Models\TaskScheduler\TaskTest::taskFails', 'P1D', new \DateTime());

        $settings = TaskSchedulerSettings::singleton();

        $count = 1;
        while ($count < $settings->maxAttempts + 2) {
            Task::processTasks();
            if ($count == $settings->maxAttempts + 1) {
                $this->assertEquals($settings->maxAttempts, TaskLog::findLast()->Attempts);
            } else {
                $this->assertEquals($count, TaskLog::findLast()->Attempts);
            }
            $count++;
        }
    }

    public function testTaskLogging()
    {
        $dateTime = new \DateTime();
        $task = Task::installTask('\GcdTech\Scheduler\Tests\Models\TaskScheduler\TaskTest::testMethod', 'P1D', $dateTime);
        $initialLogs = count(TaskLog::find(new Equals('TaskID', $task->getUniqueIdentifier())));
        Task::processTasks();
        $this->assertCount($initialLogs + 1, TaskLog::find(new Equals('TaskID', $task->getUniqueIdentifier())));
    }

    public function testTasksReschedule()
    {
        $dateTime = new \DateTime();
        $interval = 'P1D';
        $task = Task::installTask('\GcdTech\Scheduler\Tests\Models\TaskScheduler\TaskTest::testMethod', $interval, $dateTime);
        Task::processTasks();
        $finishedDateTime = new \DateTime();

        $this->assertCount(1, Task::find(new AndGroup([new Equals('TaskID', $task->getUniqueIdentifier()), new Equals('NextRun', $finishedDateTime->add(new \DateInterval($interval)))])));

        $dateTime = new \DateTime();
        $task = Task::installTask('\GcdTech\Scheduler\Tests\Models\TaskScheduler\TaskTest::testMethod', '', $dateTime);
        Task::processTasks();

        $this->assertCount(1, Task::find(new AndGroup([new Equals('TaskID', $task->getUniqueIdentifier()), new Equals('NextRun', $dateTime)])), 'Task scheduler should not try to re-assign the task to run again as there is no time interval');

        $failTaskStartDate = new \DateTime();
        $failInterval = 'P10D';
        $failedTask = Task::installTask('\GcdTech\Scheduler\Tests\Models\TaskScheduler\TaskTest::taskFails', $failInterval, $failTaskStartDate);

        $settings = TaskSchedulerSettings::singleton();

        $count = 1;
        while ($count < $settings->maxAttempts + 2) {
            Task::processTasks();
            if ($count == $settings->maxAttempts + 1) {
                $finishedDateTime = new \DateTime();
                $this->assertCount(1, Task::find(new AndGroup([new Equals('TaskID', $failedTask->getUniqueIdentifier()), new Equals('NextRun', $finishedDateTime->add(new \DateInterval($failInterval)))])));
            }
            $count++;
        }

        $task = Task::installTask('\GcdTech\Scheduler\Tests\Models\TaskScheduler\TaskTest::testMethod', $interval, $dateTime, Task::INTERVAL_FIXED_TIME);
        sleep(2);

        Task::processTasks();
        $this->assertCount(1, Task::find(new AndGroup([new Equals('TaskID', $task->getUniqueIdentifier()), new Equals('NextRun', $dateTime->add(new \DateInterval($interval)))])));
    }

    public function testTaskStatus()
    {
        $dateTime = new \DateTime();
        $task = Task::installTask('\GcdTech\Scheduler\Tests\Models\TaskScheduler\TaskTest::testMethod', '', $dateTime);
        $this->assertEquals(Task::TASK_STATUS_QUEUED, $task->Status);

        Task::processTasks();

        $task->reload();
        $this->assertEquals(Task::TASK_STATUS_DISABLED, $task->Status);

        $dateTime = new \DateTime();
        $task = Task::installTask('\GcdTech\Scheduler\Tests\Models\TaskScheduler\TaskTest::testMethod', 'P1D', $dateTime);
        Task::processTasks();

        $this->assertEquals(Task::TASK_STATUS_QUEUED, $task->Status);
    }

    /**
     * @return string
     */
    public static function testMethod()
    {
        self::$processedValue = 'processed';
    }

    public static function taskFails()
    {
        throw new \Exception('Failure!');
    }
}