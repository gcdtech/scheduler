<?php

namespace GcdTech\Scheduler\CustardCommands;

trait LoopedCommandTools
{

    protected $sleepInterval = 5;

    /**
     * @var bool Use to loop the command until a minute.
     * Used in combination with $sleepInterval this allows.
     * If you cron this job to run every minute, this will then enable "real time" task execution
     */
    protected $loopTillMinute = true;

    /**
     * @return bool
     */
    protected function shouldLoop()
    {
        // if we just want to loop forever, or we are not within 2 sleepIntervals of the minute, we should loop
        return $this->loopTillMinute && (int)date('s') + ($this->sleepInterval * 2) < 60;
    }

    /**
     * @param callable $callable
     */
    protected function executeLooped($callable) {
        $doLoop = true;
        while ($doLoop) {
            $callable();
            $doLoop = $this->shouldLoop();
            if ($doLoop) {
                sleep($this->sleepInterval);
            }
        }
    }
}
