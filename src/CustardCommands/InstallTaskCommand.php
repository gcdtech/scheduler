<?php


namespace GcdTech\Scheduler\CustardCommands;

use Rhubarb\Stem\Custard\RequiresRepositoryCommand;
use Rhubarb\Stem\Repositories\MySql\MySql;
use GcdTech\Scheduler\Models\Task;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * Class InstallTaskCommand
 * @package Somervilles\WebApp\Custard
 */
class InstallTaskCommand extends RequiresRepositoryCommand
{
    protected function configure()
    {
        $this->setName('task-scheduler:install-task')->setDescription('Installs a new task and schedules it to be run');
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        parent::interact($input, $output);
        $helper = $this->getHelper('question');

        $methodQuestion = new Question('Which method would you like to install a task for?');
        $methodQuestion->setAutocompleterValues(MySql::executeStatement("SELECT `MethodName` FROM `tblTask`")->fetchAll(\PDO::FETCH_COLUMN, 0));
        $methodQuestion->setValidator(function ($value) {
            if ($value === null) {
                throw new \RuntimeException('You must specify a method');
            }
            $methodParts = explode('::', $value);
            if (!method_exists($methodParts[0], $methodParts[1])) {
                throw new \RuntimeException('Method not found');
            }
            return $value;
        });
        $method = $helper->ask($input, $output, $methodQuestion);

        $descriptionQuestion = new Question('Please provide a description for this task: ');
        $description = $helper->ask($input, $output, $descriptionQuestion);

        $intervalQuestion = new Question('Please provide a php DateInterval string for the repetition of this task (leave blank for a one off task): ');
        $intervalQuestion->setAutocompleterValues(['P1D', 'P7D', 'PT1H', 'PT6H', 'PT12H']);
        $intervalQuestion->setValidator(function ($interval) {
            if ($interval === null) {
                return '';
            }
            new \DateInterval($interval);
            return $interval;
        });
        $interval = $helper->ask($input, $output, $intervalQuestion);

        $nextRunQuestion = new Question(
            'Please provide a first run php DateTime string (eg "now" or "2015-12-12 11:01:16" - leave blank to not schedule this task): '
        );
        $nextRun = $helper->ask($input, $output, $nextRunQuestion);

        $intervalTypeQuestion = new Question('Please provide an Interval Type for this task: ');
        $intervalTypeQuestion->setAutocompleterValues([Task::INTERVAL_FROM_TASK_COMPLETED, Task::INTERVAL_FIXED_TIME]);
        $intervalType = $helper->ask($input, $output, $intervalTypeQuestion);

        $task = Task::installTask($method, $interval, $nextRun, $intervalType, $description);
        if ($nextRun == null) {
            $task->Status = Task::TASK_STATUS_DISABLED;
            $task->save();
        }

        $output->writeln("Task #{$task->getUniqueIdentifier()} installed");
    }
}