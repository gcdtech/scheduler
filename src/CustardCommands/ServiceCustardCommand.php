<?php

namespace GcdTech\Scheduler\CustardCommands;

use Rhubarb\Custard\Command\CustardCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class ServiceCustardCommand extends CustardCommand
{
    use LockedCommandTools, LoopedCommandTools;

    final protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executeLocked(function() use ($input, $output) {
            $this->executeLooped(function() use ($output, $input) {
                $this->runService($input, $output);
            });
        });
    }

    abstract public function runService(InputInterface $input, OutputInterface $output);
}
