<?php

namespace GcdTech\Scheduler\CustardCommands;

use Rhubarb\Crown\String\StringTools;

trait LockedCommandTools
{
    /**
     * @param callable $callable
     */
    final protected function executeLocked($callable)
    {
        $file = $this->getLockFile();
        $this->acquireLock($file);
        $callable();
        $this->releaseLock($file);
        $this->handleFinished();
    }

    /**
     * @return string
     */
    protected function getLockFile()
    {
        return sys_get_temp_dir() . '/' . StringTools::getShortClassNameFromNamespace(static::class) . '.pid';
    }

    /**
     * @param string $file
     */
    private function acquireLock($file)
    {
        // Check to ensure any existing lock file is still running
        if (file_exists($file)) {
            $processID = (int)file_get_contents($file);
            if ((int)`ps {$processID}|grep php|wc -l` === 1) {
                $this->handleAlreadyRunning();
                die(0);
            }
        }

        file_put_contents($file, getmypid());
    }

    protected function handleAlreadyRunning()
    {
        die('Service already running');
    }

    protected function handleFinished()
    {
        die(0);
    }

    /**
     * @param string $file
     */
    private function releaseLock($file)
    {
        unlink($file);
    }
}
