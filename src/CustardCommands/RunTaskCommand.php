<?php

namespace GcdTech\Scheduler\CustardCommands;

use GcdTech\Scheduler\Models\Task;
use Rhubarb\Stem\Custard\RequiresRepositoryCommand;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\Not;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * Class RunTaskCommand
 * @package Somervilles\WebApp\Custard
 */
class RunTaskCommand extends RequiresRepositoryCommand
{
    protected $choices;

    private $taskID;

    protected function configure()
    {
        $this->setName('task-scheduler:run-task')
            ->setDescription('Runs an individual task')
            ->addArgument('TaskID', InputArgument::OPTIONAL, 'Specify a task to run');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
        $this->taskID = $input->getArgument('TaskID');
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        parent::interact($input, $output);

        if ($this->taskID === null) {
            $helper = $this->getHelper('question');

            $tasks = Task::find(new Not(new Equals('Status', 'Disabled')));
            $this->choices = [];
            foreach ($tasks as $task) {
                $taskValue = "Task #{$task->TaskID} - {$task->MethodName}";
                $taskValue = $task->Description ? $taskValue . " - {$task->Description}" : $taskValue;
                $this->choices[$task->TaskID] = $taskValue;
            }

            $taskQuestion = new ChoiceQuestion('Which task would you like to run now?', $this->choices);
            $taskQuestion->setValidator(function ($value) {
                if ($value === null) {
                    throw new \RuntimeException('You must specify a task');
                }

                foreach ($this->choices as $key => $choice) {
                    if ($choice === $value) {
                        $value = $key;
                    }
                }

                return $value;
            });

            $this->taskID = $helper->ask($input, $output, $taskQuestion);
        }
    }

    protected function executeWithConnection(InputInterface $input, OutputInterface $output)
    {
        parent::executeWithConnection($input, $output);

        if ($this->taskID) {
            $task = new Task($this->taskID);
            $task->processTask($output);
            $output->writeln("Task $this->taskID ({$task->MethodName}) successfully ran");
        }
    }

}
