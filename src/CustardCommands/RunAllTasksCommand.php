<?php

namespace GcdTech\Scheduler\CustardCommands;

use GcdTech\Scheduler\Models\Task;
use Rhubarb\Stem\Custard\RequiresRepositoryCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * Class RunAllTasksCommand
 * @package Somervilles\WebApp\Custard
 */
class RunAllTasksCommand extends RequiresRepositoryCommand
{
    private $run = false;

    protected function configure()
    {
        $this->setName('task-scheduler:run-all-tasks')
            ->setDescription('Runs all installed tasks')
            ->addOption('f', null, null, 'Forces run - does not prompt user');
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        parent::interact($input, $output);
        $helper = $this->getHelper('question');

        if (!$this->run) {
            $this->run = $helper->ask($input, $output, new Question('Run all tasks now? [y/n] ', 'y')) === 'y';
        }
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
        $this->run = $input->getOption('f');
    }

    protected function executeWithConnection(InputInterface $input, OutputInterface $output)
    {
        parent::executeWithConnection($input, $output);
        if ($this->run) {
            Task::processTasks($output);
        } else {
            $output->writeln("No tasks ran at this time");
        }
    }
}
