<?php

namespace GcdTech\Scheduler\Models;

use GcdTech\Scheduler\TaskSchedulerSettings;
use Rhubarb\Stem\Exceptions\RecordNotFoundException;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\LessThan;
use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Repositories\MySql\Schema\Columns\MySqlEnumColumn;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\DateTimeColumn;
use Rhubarb\Stem\Schema\Columns\StringColumn;
use Rhubarb\Stem\Schema\ModelSchema;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @property int $TaskID Repository field
 * @property string $MethodName Repository field
 * @property string $DateInterval Repository field
 * @property string $NextRun Repository field
 * @property string $IntervalType Repository field
 * @property string $Status Repository field
 * @property string $Description Repository field
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * Class Task
 */
class Task extends Model
{
    const INTERVAL_FROM_TASK_COMPLETED = 'FromTaskCompleted';
    const INTERVAL_FIXED_TIME = 'FixedTime';

    const TASK_STATUS_QUEUED = 'Queued';
    const TASK_STATUS_RUNNING = 'Running';
    const TASK_STATUS_DISABLED = 'Disabled';

    /** @var  TaskLog */
    private $log;

    /**
     * Returns the schema for this data object.
     *
     * @return \Rhubarb\Stem\Schema\ModelSchema
     */
    protected function createSchema()
    {
        $schema = new ModelSchema('tblTask');
        $schema->addColumn(
            new AutoIncrementColumn('TaskID'),
            new StringColumn('MethodName', 255),
            new StringColumn('DateInterval', 20),
            new DateTimeColumn('NextRun'),
            new MySqlEnumColumn('IntervalType', Task::INTERVAL_FROM_TASK_COMPLETED,
                [Task::INTERVAL_FROM_TASK_COMPLETED, Task::INTERVAL_FIXED_TIME]),
            new MySqlEnumColumn('Status', Task::TASK_STATUS_QUEUED,
                [Task::TASK_STATUS_QUEUED, Task::TASK_STATUS_RUNNING, Task::TASK_STATUS_DISABLED]),
            new StringColumn('Description', 255)
        );

        return $schema;
    }

    /**
     * @param $methodName
     * @param string $dateInterval
     * @param string $nextRun
     * @param string $intervalType
     * @param string $description
     * @return Task
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     */
    public static function installTask(
        $methodName,
        $dateInterval = '',
        $nextRun = null,
        $intervalType = self::INTERVAL_FROM_TASK_COMPLETED,
        $description = ''
    ) {
        $task = new Task();
        $task->MethodName = $methodName;
        $task->DateInterval = $dateInterval;
        $task->NextRun = $nextRun;
        $task->IntervalType = $intervalType;
        $task->Description = $description;
        $task->save();

        return $task;
    }

    /**
     * @param OutputInterface|null $output
     */
    public static function processTasks(OutputInterface $output = null)
    {
        /** @var Task[] $tasks */
        $tasks = Task::find(new AndGroup(
            new Equals('Status', self::TASK_STATUS_QUEUED),
            new LessThan('NextRun', (new \DateTime())->format('Y-m-d H:i:s'), true)
        ));
        foreach ($tasks as $task) {
            $task->processTask($output);
        }
    }

    public function processTask(OutputInterface $output = null)
    {
        $this->createLog($output);
        $this->log->addInfo('Task Started');
        $this->log->Attempts++;
        $this->log->save();
        $this->process();
    }

    private function process()
    {
        try {
            $method = $this->MethodName;
            $this->log->addInfo("Action Started - {$this->MethodName}");
            $this->Status = Task::TASK_STATUS_RUNNING;
            $this->save();
            $method($this->log);
            $this->onSuccess();
        } catch (\Exception $ex) {
            $this->log->addError($ex->getMessage());
            $this->onError();
        }
    }

    private function createLog(OutputInterface $output = null)
    {
        try {
            $this->log =
                TaskLog::findFirst(new AndGroup([
                    new Equals('TaskID', $this->getUniqueIdentifier()),
                    new Equals('Completed', 0),
                ]));
        } catch (RecordNotFoundException $ex) {
            $this->log = new TaskLog();
            $this->log->TaskID = $this->getUniqueIdentifier();
            $this->log->save();
        }
        $this->log->setOutputInterface($output);
    }

    public function onSuccess()
    {
        $this->log->Success = true;
        $this->log->save();

        $this->markCompleted();
    }

    public function onError()
    {
        $settings = TaskSchedulerSettings::singleton();
        if ($this->log->Attempts == $settings->maxAttempts) {
            $this->markCompleted();
        }
    }

    public function markCompleted()
    {
        $this->log->addInfo('Task Completed');
        $this->log->Completed = true;
        $this->log->save();

        $this->rescheduleTask();
    }

    public function rescheduleTask()
    {
        if ($this->DateInterval != '') {
            $this->Status = Task::TASK_STATUS_QUEUED;
            if ($this->IntervalType == Task::INTERVAL_FROM_TASK_COMPLETED) {
                $finishedDate = new \DateTime();
                $this->NextRun = $finishedDate->add(new \DateInterval($this->DateInterval));
            } else {
                $this->NextRun = $this->NextRun->add(new \DateInterval($this->DateInterval));
            }
        } else {
            $this->Status = Task::TASK_STATUS_DISABLED;
        }
        $this->save(true);
    }
}
