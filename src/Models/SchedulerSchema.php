<?php

namespace GcdTech\Scheduler\Models;

use Rhubarb\Crown\String\StringTools;
use Rhubarb\Stem\Schema\SolutionSchema;

class SchedulerSchema extends SolutionSchema
{
    public function __construct($version = 1)
    {
        parent::__construct($version);
        $this->addModel(StringTools::getShortClassNameFromNamespace(Task::class), Task::class, 1);
        $this->addModel(StringTools::getShortClassNameFromNamespace(TaskLog::class), TaskLog::class, 1);
    }
}