<?php

namespace GcdTech\Scheduler\Models;

use GcdTech\Scheduler\TaskSchedulerSettings;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\TestHandler;
use Monolog\Logger;
use Rhubarb\Crown\Application;
use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\BooleanColumn;
use Rhubarb\Stem\Schema\Columns\ForeignKeyColumn;
use Rhubarb\Stem\Schema\Columns\IntegerColumn;
use Rhubarb\Stem\Schema\ModelSchema;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @property int $TaskLogID Repository field
 * @property int $TaskID Repository field
 * @property bool $Success Repository field
 * @property bool $Completed Repository field
 * @property int $Attempts Repository field
 *
 * Class TaskLog
 * @package Somervilles\WebApp\Models\TaskScheduler
 */
class TaskLog extends Model
{
    /** @var Logger */
    private $logger;

    /** @var OutputInterface */
    private $outputInterface;

    /**
     * Returns the schema for this data object.
     *
     * @return \Rhubarb\Stem\Schema\ModelSchema
     */
    protected function createSchema()
    {
        $schema = new ModelSchema('tblTaskLog');
        $schema->addColumn(
            new AutoIncrementColumn('TaskLogID'),
            new ForeignKeyColumn('TaskID'),
            new BooleanColumn('Success'),
            new IntegerColumn('Attempts'),
            new BooleanColumn('Completed')
        );

        return $schema;
    }

    private function prepareLogger()
    {
        if ($this->logger === null) {
            $this->logger = new Logger('TaskLog');
            $application = Application::current();
            $settings = TaskSchedulerSettings::singleton();
            if ($application->isUnitTesting()) {
                $handler = new TestHandler();
            } else {
                $directory = APPLICATION_ROOT_DIR . '/log/scheduler';
                if (!is_dir($directory)) {
                    mkdir($directory, 0777, true);
                }
                $handler = new StreamHandler($directory . "/{$this->TaskLogID}.txt", $settings->logLevel);
            }
            $this->logger->pushHandler($handler);
        }
    }

    /**
     * @param string $message
     */
    public function addInfo($message = '')
    {
        $this->prepareLogger();
        if ($this->outputInterface !== null) {
            $this->outputInterface->writeln("<info>{$message}</info>");
        }
        $this->logger->addInfo($message);
    }

    /**
     * @param string $message
     */
    public function addDebug($message = '')
    {
        $this->prepareLogger();
        if ($this->outputInterface !== null) {
            $this->outputInterface->writeln($message);
        }
        $this->logger->addDebug($message);
    }

    /**
     * @param string $message
     */
    public function addWarning($message = '')
    {
        $this->prepareLogger();
        if ($this->outputInterface !== null) {
            $this->outputInterface->writeln("<comment>{$message}</comment>");
        }
        $this->logger->addWarning($message);
    }

    /**
     * @param string $message
     */
    public function addError($message = '')
    {
        $this->prepareLogger();
        if ($this->outputInterface !== null) {
            $this->outputInterface->writeln("<error>{$message}</error>");
        }
        $this->logger->addError($message);
    }

    /**
     * @param OutputInterface|null $output
     */
    public function setOutputInterface(OutputInterface $output = null)
    {
        $this->outputInterface = $output;
    }
}
