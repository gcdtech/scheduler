<?php

namespace GcdTech\Scheduler;

use Monolog\Logger;
use Rhubarb\Crown\Settings;

/**
 * Class TaskSchedulerSettings
 * @package Somervilles\WebApp\Models\TaskScheduler
 */
class TaskSchedulerSettings extends Settings
{
    public $logLevel;
    public $maxAttempts;
    public $downTime;
    public $targetRunLength;

    protected function initialiseDefaultValues()
    {
        parent::initialiseDefaultValues();
        $this->logLevel = Logger::WARNING;
        $this->maxAttempts = 10;
        $this->downTime = 10;
        $this->targetRunLength = 5;
    }
}
