<?php

namespace GcdTech\Scheduler;

use GcdTech\Scheduler\CustardCommands\InstallTaskCommand;
use GcdTech\Scheduler\CustardCommands\RunAllTasksCommand;
use GcdTech\Scheduler\CustardCommands\RunTaskCommand;
use GcdTech\Scheduler\Models\SchedulerSchema;
use Rhubarb\Crown\Module;
use Rhubarb\Stem\Schema\SolutionSchema;
use Rhubarb\Stem\StemModule;

/**
 * Class SchedulerModule
 */
class SchedulerModule extends Module
{
    public function initialise()
    {
        SolutionSchema::registerSchema('Scheduler', SchedulerSchema::class);
    }

    public function getCustardCommands()
    {
        $commands = parent::getCustardCommands();
        $commands[] = new InstallTaskCommand();
        $commands[] = new RunAllTasksCommand();
        $commands[] = new RunTaskCommand();
        return $commands;
    }

    protected function getModules()
    {
        return [new StemModule()];
    }
}